<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//group route with prefix "admin"
Route::prefix('admin')->group(function () {
    //route login
    Route::post('/login', [App\Http\Controllers\Api\Admin\LoginController::class, 'index']);
    //group route with middleware "auth"
    Route::group(['middleware' => 'auth:api'], function() {
        //data user
        Route::get('/user', [App\Http\Controllers\Api\Admin\LoginController::class, 'getUser']);
        //refresh token JWT
        Route::get('/refresh', [App\Http\Controllers\Api\Admin\LoginController::class, 'refreshToken']);
        //logout
        Route::post('/logout', [App\Http\Controllers\Api\Admin\LoginController::class, 'logout']);
        Route::apiResource('/tags', App\Http\Controllers\Api\Admin\TagController::class);
        Route::apiResource('/categories', App\Http\Controllers\Api\Admin\CategoryController::class);
        Route::apiResource('/posts', App\Http\Controllers\Api\Admin\PostController::class);
        Route::apiResource('/menus', App\Http\Controllers\Api\Admin\MenuController::class);
        Route::apiResource('/sliders', App\Http\Controllers\Api\Admin\SliderController::class);
        Route::apiResource('/users', App\Http\Controllers\Api\Admin\UserController::class);
        Route::get('/dashboard', [App\Http\Controllers\Api\Admin\DashboardController::class, 'index']);
    });

});

Route::prefix('web')->group(function () {
    //index tags
    Route::get('/tags', [App\Http\Controllers\Api\Web\TagController::class, 'index']);
    //show tag
    Route::get('/tags/{slug}', [App\Http\Controllers\Api\Web\TagController::class, 'show']);
    //index categories
    Route::get('/categories', [App\Http\Controllers\Api\Web\CategoryController::class, 'index']);
    //show category
    Route::get('/categories/{slug}', [App\Http\Controllers\Api\Web\CategoryController::class, 'show']);
    //categories sidebar
    Route::get('/categorySidebar', [App\Http\Controllers\Api\Web\CategoryController::class, 'categorySidebar']);
    //index posts
    Route::get('/posts', [App\Http\Controllers\Api\Web\PostController::class, 'index']);
    //show posts
    Route::get('/posts/{slug}', [App\Http\Controllers\Api\Web\PostController::class, 'show']);
    //posts homepage
    Route::get('/postHomepage', [App\Http\Controllers\Api\Web\PostController::class, 'postHomepage']);
    //store comment
    Route::post('/posts/storeComment', [App\Http\Controllers\Api\Web\PostController::class, 'storeComment']);
    //store image
    Route::post('/posts/storeImage', [App\Http\Controllers\Api\Web\PostController::class, 'storeImagePost']);
    //index menus
    Route::get('/menus', [App\Http\Controllers\Api\Web\MenuController::class, 'index']);
    Route::get('/sliders', [App\Http\Controllers\Api\Web\SliderController::class, 'index']);
});
